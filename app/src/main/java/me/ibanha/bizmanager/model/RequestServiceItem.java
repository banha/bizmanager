package me.ibanha.bizmanager.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.TypeConverters;

import java.util.List;

public class RequestServiceItem {

    private String date;
    private String time;
    @ColumnInfo(name = "user_uid")
    private String uid;
    @ColumnInfo(name = "user_name")
    private String name;
    @ColumnInfo(name = "user_email")
    private String email;
    @ColumnInfo(name = "user_phone")
    private String phone;
    @ColumnInfo(name = "user_discount")
    private Byte discount;
    @TypeConverters({ServiceConverter.class})
    private List<String> service;
    private boolean approved;

    public RequestServiceItem(String date, String time, String uid, String name, String email, String phone, Byte discount, List<String> service, boolean approved) {
        this.date = date;
        this.time = time;
        this.uid = uid;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.discount = discount;
        this.service = service;
        this.approved = approved;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Byte getDiscount() {
        return discount;
    }

    public void setDiscount(Byte discount) {
        this.discount = discount;
    }

    public List<String> getService() {
        return service;
    }

    public void setService(List<String> service) {
        this.service = service;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
}
