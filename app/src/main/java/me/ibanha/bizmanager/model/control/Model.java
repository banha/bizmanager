package me.ibanha.bizmanager.model.control;

import java.util.ArrayList;
import java.util.List;

import me.ibanha.bizmanager.model.Service;

public class Model {
    private String date;
    private String time;
    private String uid;
    private String name;
    private String email;
    private String phone;
    private Byte discount;
    private List<Service> services;
    private String duration;
    private String buildedServicesIds;
    private long pricesWithDiscount;
    private long pricesWithoutDiscount;
    private boolean approved;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Byte getDiscount() {
        return discount;
    }

    public void setDiscount(Byte discount) {
        this.discount = discount;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public List<String> getServiceIds() {
        ArrayList<String> ids = new ArrayList<>();
        for (int i = 0; services.size() > i; i++) {
            ids.add(services.get(i).getId());
        }
        return ids;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public String getBuildedServicesIds() {
        return buildedServicesIds;
    }

    public void setBuildedServicesIds(String buildedServicesIds) {
        this.buildedServicesIds = buildedServicesIds;
    }

    public long getPricesWithDiscount() {
        return pricesWithDiscount;
    }

    public void setPricesWithDiscount(long pricesWithDiscount) {
        this.pricesWithDiscount = pricesWithDiscount;
    }

    public long getPricesWithoutDiscount() {
        return pricesWithoutDiscount;
    }

    public void setPricesWithoutDiscount(long pricesWithoutDiscount) {
        this.pricesWithoutDiscount = pricesWithoutDiscount;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
