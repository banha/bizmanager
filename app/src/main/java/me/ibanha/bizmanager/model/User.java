package me.ibanha.bizmanager.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class User {
    @PrimaryKey
    @NonNull
    private String uid;
    @NonNull
    private String name;
    @NonNull
    private String email;
    @NonNull
    private String phone;
    @NonNull
    private Integer discount;
    @NonNull
    private boolean isAdmin;
    private String password;

    public User() {
    }

    public User(@NonNull String uid, @NonNull String name, @NonNull String email,
                @NonNull String phone, @NonNull Integer discount, @NonNull boolean isAdmin) {
        this.uid = uid;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.discount = discount;
        this.isAdmin = isAdmin;
    }

    public User(@NonNull String uid, @NonNull String name, @NonNull String email,
                @NonNull String phone, @NonNull Integer discount, @NonNull boolean isAdmin, String password) {
        this.uid = uid;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.discount = discount;
        this.isAdmin = isAdmin;
        this.password = password;
    }

    @NonNull
    public String getUid() {
        return uid;
    }

    public void setUid(@NonNull String uid) {
        this.uid = uid;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getEmail() {
        return email;
    }

    public void setEmail(@NonNull String email) {
        this.email = email;
    }

    @NonNull
    public String getPhone() {
        return phone;
    }

    public void setPhone(@NonNull String phone) {
        this.phone = phone;
    }

    @NonNull
    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(@NonNull Integer discount) {
        this.discount = discount;
    }

    @NonNull
    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(@NonNull boolean admin) {
        isAdmin = admin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
