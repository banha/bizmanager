package me.ibanha.bizmanager.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import java.util.List;

@Entity
public class Request {
    @PrimaryKey
    @NonNull
    private String id;
    @NonNull
    private String date;
    @NonNull
    private String time;
    @NonNull
    @ColumnInfo(name = "user_uid")
    private String client;
    @NonNull
    @TypeConverters({ServiceConverter.class})
    private List<String> service;
    @NonNull
    private boolean approved;

    public Request() {
    }

    public Request(@NonNull String id, @NonNull String date, @NonNull String time, @NonNull String client, @NonNull List<String> service, @NonNull boolean approved) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.client = client;
        this.service = service;
        this.approved = approved;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    @NonNull
    public String getDate() {
        return date;
    }

    public void setDate(@NonNull String date) {
        this.date = date;
    }

    @NonNull
    public String getTime() {
        return time;
    }

    public void setTime(@NonNull String time) {
        this.time = time;
    }

    @NonNull
    public String getClient() {
        return client;
    }

    public void setClient(@NonNull String client) {
        this.client = client;
    }

    @NonNull
    public List<String> getService() {
        return service;
    }

    public void setService(@NonNull List<String> service) {
        this.service = service;
    }

    @NonNull
    public boolean isApproved() {
        return approved;
    }

    public void setApproved(@NonNull boolean approved) {
        this.approved = approved;
    }
}
