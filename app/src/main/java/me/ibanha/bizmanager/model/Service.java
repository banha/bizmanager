package me.ibanha.bizmanager.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Service {
    @NonNull
    @PrimaryKey
    private String id;
    @NonNull
    private Long price;
    @NonNull
    private Long cost;
    @NonNull
    private String duration;
    @NonNull
    private String description;

    public Service() {
    }

    public Service(@NonNull String id, @NonNull Long price, @NonNull Long cost,
                   @NonNull String duration, @NonNull String description) {
        this.id = id;
        this.price = price;
        this.cost = cost;
        this.duration = duration;
        this.description = description;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    @NonNull
    public Long getPrice() {
        return price;
    }

    public void setPrice(@NonNull Long price) {
        this.price = price;
    }

    @NonNull
    public Long getCost() {
        return cost;
    }

    public void setCost(@NonNull Long cost) {
        this.cost = cost;
    }

    @NonNull
    public String getDuration() {
        return duration;
    }

    public void setDuration(@NonNull String duration) {
        this.duration = duration;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NonNull String description) {
        this.description = description;
    }
}
