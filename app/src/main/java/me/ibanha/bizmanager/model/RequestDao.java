package me.ibanha.bizmanager.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import java.util.List;

@Dao
public interface RequestDao {
    @Insert
    void insertAll(List<Request> items);

    @Insert
    void insert(Request item);

    @Query("DELETE FROM Request")
    void clearTable();

    @Query("DELETE FROM Request WHERE id IN (:id)")
    void deleteById(String id);

    @Query("SELECT * FROM Request")
    List<Request> getItems();

    @Query("SELECT * FROM Request WHERE id IN (:id)")
    Request getItemById(String id);

    @Query("SELECT * FROM Request WHERE date IN (:date)")
    Request getItemByDate(String date);

    @Query("SELECT * FROM Request WHERE approved IN (:status)")
    List<Request> getItemByStatus(Boolean status);

    @Transaction
    @Query("SELECT request.date, request.time, request.approved, request.service ," +
            "user.email AS user_email, user_uid AS user_uid, user.name AS user_name," +
            " user.phone AS user_phone, user.discount AS user_discount " +
            "FROM request, user " +
            "WHERE user.uid == request.user_uid AND approved IN (:state) AND date IN (:date)")
    List<RequestServiceItem> getRequestsByDate(String date, Boolean state);

    @Transaction
    @Query("SELECT request.date, request.time, request.approved, request.service ," +
            "user.email AS user_email, user_uid AS user_uid, user.name AS user_name," +
            " user.phone AS user_phone, user.discount AS user_discount " +
            "FROM request, user " +
            "WHERE user.uid == request.user_uid AND user_uid IN (:user)")
    List<RequestServiceItem> getRequestsByUser(String user);

    @Transaction
    @Query("SELECT request.date, request.time, request.approved, request.service ," +
            "user.email AS user_email, user_uid AS user_uid, user.name AS user_name," +
            " user.phone AS user_phone, user.discount AS user_discount " +
            "FROM request, user " +
            "WHERE user.uid == request.user_uid AND request.id IN (:id) AND approved IN (:type) ")
    List<RequestServiceItem> getRequestByIdAndType(String id, boolean type);

    @Query("SELECT date FROM Request WHERE approved IN (:state)")
    List<String> getItemsDate(Boolean state);
}
