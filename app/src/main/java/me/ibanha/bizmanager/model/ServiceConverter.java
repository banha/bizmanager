package me.ibanha.bizmanager.model;

import android.arch.persistence.room.TypeConverter;
import android.os.Build;

import com.annimon.stream.Stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ServiceConverter {
    @TypeConverter
    public String fromServices(List<String> services) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return services.stream().collect(Collectors.joining(","));
        } else {
            return Stream.of(services).collect(com.annimon.stream.Collectors.joining(","));
        }
    }

    @TypeConverter
    public List<String> toServices(String data) {
        return Arrays.asList(data.split(","));
    }
}
