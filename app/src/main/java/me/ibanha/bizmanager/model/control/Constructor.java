package me.ibanha.bizmanager.model.control;

import java.util.ArrayList;
import java.util.List;

import me.ibanha.bizmanager.database.AppManager;
import me.ibanha.bizmanager.helper.Helper;
import me.ibanha.bizmanager.model.RequestServiceItem;
import me.ibanha.bizmanager.model.Service;
import me.ibanha.bizmanager.model.ServiceDao;
import me.ibanha.bizmanager.model.User;

public class Constructor {
    private ArrayList<RequestServiceItem> items;
    private RequestServiceItem item;
    private Helper helper = new Helper();

    public Constructor() {
    }

    public Constructor(String uid) {
        items = (ArrayList<RequestServiceItem>) AppManager.getInstance().getRoom().getRequestDao()
                .getRequestsByUser(uid);
    }

    public Constructor(String date, boolean type) {
        items = (ArrayList<RequestServiceItem>) AppManager.getInstance().getRoom().getRequestDao()
                .getRequestsByDate(date, type);
    }

    public void init(String id, Boolean type) {
        item = AppManager.getInstance().getRoom().getRequestDao().getRequestByIdAndType(id, type).get(0);
    }

    public ArrayList<Model> getModels() {
        ArrayList<Model> items = new ArrayList<>();
        ServiceDao dao = AppManager.getInstance().getRoom().getServiceDao();
        String uid = AppManager.getInstance().getFireDatabase().getUserUid();
        User currentUser = AppManager.getInstance().getRoom().getUserDao().getByUid(uid);

        for (int position = 0; position < this.items.size(); position++) {
            List<String> ids = this.items.get(position).getService();
            ArrayList<Service> services = new ArrayList<>();

            for (int i = 0; i < ids.size(); i++) {
                services.add(dao.getById(ids.get(i)));
            }

            Model model = new Model();
            model.setDate(this.items.get(position).getDate());
            model.setTime(this.items.get(position).getTime());
            model.setUid(this.items.get(position).getUid());
            model.setName(this.items.get(position).getName());
            model.setEmail(this.items.get(position).getEmail());
            model.setPhone(this.items.get(position).getPhone());
            model.setDiscount(this.items.get(position).getDiscount());
            model.setServices(services);
            model.setBuildedServicesIds(helper.collectTitle(this.items.get(position).getService()));
            model.setPricesWithDiscount(helper.calcSumWithDiscount(services, currentUser));
            model.setPricesWithoutDiscount(helper.calcSum(services));
            model.setApproved(this.items.get(position).isApproved());
            items.add(model);
        }
        return items;
    }

    public Model getModel() {
        Model item = new Model();
        ServiceDao dao = AppManager.getInstance().getRoom().getServiceDao();
        String uid = this.item.getUid();
        User user = AppManager.getInstance().getRoom().getUserDao().getByUid(uid);

        List<String> ids = this.item.getService();
        ArrayList<Service> services = new ArrayList<>();

        for (int i = 0; i < ids.size(); i++) {
            services.add(dao.getById(ids.get(i)));
        }

        item.setDate(this.item.getDate());
        item.setTime(this.item.getTime());
        item.setUid(user.getUid());
        item.setName(user.getName());
        item.setEmail(user.getEmail());
        item.setPhone(user.getPhone());
        item.setDiscount(Byte.parseByte(user.getDiscount().toString()));
        item.setServices(services);
        item.setDuration(helper.calculateSumOfHours(item.getServices()));
        item.setBuildedServicesIds(helper.collectTitle(this.item.getService()));
        item.setPricesWithDiscount(helper.calcSumWithDiscount(services, user));
        item.setPricesWithoutDiscount(helper.calcSum(services));
        item.setApproved(this.item.isApproved());
        return item;
    }

}
