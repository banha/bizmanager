package me.ibanha.bizmanager.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface UserDao {
    @Insert
    void insertAll(List<User> users);

    @Insert
    void insert(User user);

    @Query("DELETE FROM user")
    void clearTable();

    @Query("DELETE FROM user WHERE uid IN (:uid)")
    void deleteByUid(String uid);

    @Query("SELECT * FROM user")
    List<User> getUsers();

    @Query("SELECT * FROM user WHERE uid IN (:uid)")
    User getByUid(String uid);

    @Query("SELECT * FROM user WHERE isAdmin IN (:isAdmin)")
    User getAdmin(Boolean isAdmin);
}
