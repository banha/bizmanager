package me.ibanha.bizmanager.model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ServiceDao {
    @Insert
    void insertAll(List<Service> items);

    @Insert
    void insert(Service item);

    @Query("DELETE FROM Service")
    void clearTable();

    @Query("DELETE FROM Service WHERE id IN (:id)")
    void deleteItemById(String id);

    @Query("SELECT * FROM Service")
    List<Service> getItems();

    @Query("SELECT * FROM Service WHERE id IN (:id)")
    Service getById(String id);
}
