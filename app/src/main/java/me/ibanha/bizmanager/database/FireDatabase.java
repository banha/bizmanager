package me.ibanha.bizmanager.database;


import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.ibanha.bizmanager.model.Request;
import me.ibanha.bizmanager.model.Service;
import me.ibanha.bizmanager.model.User;

public class FireDatabase {
    private static final String TAG = FireDatabase.class.getClass().getSimpleName();

    private final String SERVICES_COLLECTION = "services";
    private final String SERVICE_ID = "title";
    private final String SERVICE_PRICE = "price";
    private final String SERVICE_COST = "cost";
    private final String SERVICE_DURATION = "duration";
    private final String SERVICE_DESCRIPTION = "description";

    private final String REQUESTS_COLLECTION = "requests";
    private final String REQUESTS_ID = "id";
    private final String REQUESTS_DATE = "date";
    private final String REQUESTS_TIME = "time";
    private final String REQUESTS_CLIENT = "client";
    private final String REQUESTS_SERVICE = "service";
    private final String REQUESTS_STATUS = "status";

    private final String USERS_COLLECTION = "users";
    private final String USER_UID = "uid";
    private final String USER_NAME = "name";
    private final String USER_EMAIL = "email";
    private final String USER_PHONE = "phone";
    private final String USER_DISCOUNT = "discount";
    private final String USER_IS_ADMIN = "isAdmin";

    private static FireDatabase instance = null;

    private FirebaseUser user;
    private FirebaseAuth auth;
    private FirebaseFirestore firestore;

    private FireDatabase() {
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        firestore = FirebaseFirestore.getInstance();
    }

    public void updateUser(FirebaseUser user) {
        this.user = user;
    }

    static FireDatabase getInstance() {
        if (instance == null) {
            synchronized (FireDatabase.class) {
                instance = new FireDatabase();
            }
        }
        return instance;
    }

    public String getUserUid() {
        return user.getUid();
    }

    public String getUserEmail() {
        return user.getEmail();
    }

    public boolean isSigned() {
        return user != null;
    }

    public void signOut() {
        if (user != null) {
            auth.signOut();
            user = auth.getCurrentUser();
            Log.i(TAG, "Sign Out! ");
        } else {
            Log.e(TAG, "Sign Out failed!");
        }
    }

    public Boolean isAdmin() {
        User user = AppManager.instance.getRoom().getUserDao().getAdmin(true);
        if (user == null | this.user == null)
            return null;
        else
            return this.user.getUid().equals(user.getUid());
    }

    public synchronized void downloadAdminInfo(final FirestoreSaverCallback callback) {
        final int SUCCESSFULLY = 1;
        final int ERROR = -1;

        final int[] params = {0};
        firestore.collection("admin").document("admin").get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            User item = new User(
                                    document.getString(USER_UID),
                                    document.getString(USER_NAME),
                                    document.getString(USER_EMAIL),
                                    document.getString(USER_PHONE),
                                    Integer.parseInt(String.valueOf(document.getLong(USER_DISCOUNT))),
                                    document.getBoolean(USER_IS_ADMIN),
                                    document.getString("password")
                            );
                            callback.saveAdmin(item);
                            params[0] = SUCCESSFULLY;
                        } else {
                            params[0] = ERROR;
                            Log.e(TAG, "Error getting users: ", task.getException());
                        }
                    }
                });
        do {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (params[0] == 0);
    }

    public synchronized void downloadServices(final FirestoreSaverCallback callback) {
        // Consistency of fields (Services):
        // String id, Long price, Long cost, String duration, String description

        final int SUCCESSFULLY = 1;
        final int ERROR = -1;

        final int[] params = {0};

        firestore.collection(SERVICES_COLLECTION).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            ArrayList<Service> items = new ArrayList<>();
                            for (DocumentSnapshot document : task.getResult()) {
                                items.add(new Service(
                                        document.getString(SERVICE_ID),
                                        document.getLong(SERVICE_PRICE),
                                        document.getLong(SERVICE_COST),
                                        document.getString(SERVICE_DURATION),
                                        document.getString(SERVICE_DESCRIPTION)
                                ));
                            }
                            callback.saveServices(items);
                            params[0] = SUCCESSFULLY;
                        } else {
                            params[0] = ERROR;
                            Log.e(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
        do {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (params[0] == 0);
    }

    public synchronized void downloadRequests(final FirestoreSaverCallback callback) {
        // Consistency of fields (Requests):
        // String id, String date, String time, String client, String service, bool status
        final int SUCCESSFULLY = 1;
        final int ERROR = -1;

        final int[] params = {0};

        firestore.collection(REQUESTS_COLLECTION).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            ArrayList<Request> items = new ArrayList<>();
                            for (DocumentSnapshot document : task.getResult()) {
                                items.add(new Request(
                                        document.getString(REQUESTS_ID),
                                        document.getString(REQUESTS_DATE),
                                        document.getString(REQUESTS_TIME),
                                        document.getString(REQUESTS_CLIENT),
                                        (List<String>) document.get(REQUESTS_SERVICE),
                                        document.getBoolean(REQUESTS_STATUS))
                                );
                            }
                            callback.saveRequests(items);
                            params[0] = SUCCESSFULLY;
                        } else {
                            params[0] = ERROR;
                            Log.e(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
        do {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (params[0] == 0);
    }

    public synchronized void downloadUsers(final FirestoreSaverCallback callback) {
        // Consistency of fields (Users):
        // String uid, String name, String email, Integer phone, Byte discount

        final int SUCCESSFULLY = 1;
        final int ERROR = -1;

        final int[] params = {0};

        firestore.collection(USERS_COLLECTION).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            ArrayList<User> items = new ArrayList<>();
                            for (DocumentSnapshot document : task.getResult()) {
                                items.add(new User(
                                        document.getString(USER_UID),
                                        document.getString(USER_NAME),
                                        document.getString(USER_EMAIL),
                                        document.getString(USER_PHONE),
                                        Integer.parseInt(String.valueOf(document.getLong(USER_DISCOUNT))),
                                        document.getBoolean(USER_IS_ADMIN)
                                ));
                            }
                            callback.saveUsers(items);
                            params[0] = SUCCESSFULLY;
                        } else {
                            params[0] = ERROR;
                            Log.e(TAG, "Error getting users: ", task.getException());
                        }
                    }
                });
        do {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (params[0] == 0);
    }

    public void insertServiceItem(Service service) {
        // Consistency of fields (Services):
        // String id, Long price, Long cost, String duration, String description

        Map<String, Object> item = new HashMap<>();
        item.put(SERVICE_ID, service.getId());
        item.put(SERVICE_PRICE, service.getPrice());
        item.put(SERVICE_COST, service.getCost());
        item.put(SERVICE_DURATION, service.getDuration());
        item.put(SERVICE_DESCRIPTION, service.getDescription());

        firestore.collection(SERVICES_COLLECTION).document(service.getId()).set(item);
    }

    public void insertRequestItem(Request request) {
        // Consistency of fields (Requests):
        // String id, String date, String time, String client, String service, bool status

        Map<String, Object> item = new HashMap<>();
        item.put(REQUESTS_ID, request.getId());
        item.put(REQUESTS_DATE, request.getDate());
        item.put(REQUESTS_TIME, request.getTime());
        item.put(REQUESTS_CLIENT, request.getClient());
        item.put(REQUESTS_SERVICE, request.getService());
        item.put(REQUESTS_STATUS, request.isApproved());

        firestore.collection(REQUESTS_COLLECTION).document(request.getId()).set(item);
    }

    public void insertAdminInfo(User user) {
        Map<String, Object> item = new HashMap<>();
        item.put(USER_UID, user.getUid());
        item.put(USER_NAME, user.getName());
        item.put(USER_EMAIL, user.getEmail());
        item.put(USER_PHONE, user.getPhone());
        item.put(USER_DISCOUNT, user.getDiscount());
        item.put(USER_IS_ADMIN, user.isAdmin());

        firestore.collection("admin").document("admin").set(item);
    }

    public void insertUserItem(User user) {
        // Consistency of fields (Users):
        // String uid, String name, String email, Integer phone, Byte discount

        Map<String, Object> item = new HashMap<>();
        item.put(USER_UID, user.getUid());
        item.put(USER_NAME, user.getName());
        item.put(USER_EMAIL, user.getEmail());
        item.put(USER_PHONE, user.getPhone());
        item.put(USER_DISCOUNT, user.getDiscount());
        item.put(USER_IS_ADMIN, user.isAdmin());

        firestore.collection(USERS_COLLECTION).document(user.getUid()).set(item);
    }

    public void deleteRequestItemById(String id) {
        firestore.collection(REQUESTS_COLLECTION).document(id).delete().
                addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "Request item deleted from firestore!");
                    }
                });
    }

    public void deleteServiceItemById(String id) {
        firestore.collection(SERVICES_COLLECTION).document(id)
                .delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "Service item deleted from firestore!");
            }
        });
    }

    public void deleteUserItemById(User user) {
        firestore.collection(USERS_COLLECTION).document(user.getUid())
                .delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "Service item deleted from firestore!");
            }
        });
    }


}
