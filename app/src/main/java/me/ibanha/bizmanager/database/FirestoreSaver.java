package me.ibanha.bizmanager.database;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import me.ibanha.bizmanager.model.Request;
import me.ibanha.bizmanager.model.RequestDao;
import me.ibanha.bizmanager.model.Service;
import me.ibanha.bizmanager.model.ServiceDao;
import me.ibanha.bizmanager.model.User;
import me.ibanha.bizmanager.model.UserDao;

public class FirestoreSaver implements FirestoreSaverCallback {
    @Override
    public synchronized void saveServices(ArrayList<Service> services) {
        ServiceDao dao = AppManager.getInstance().getRoom().getServiceDao();
        for (int i = 0; services.size() > i; i++) {
            Service newest = services.get(i);
            Service older = dao.getById(newest.getId());
            if (!compareServices(newest, older)) {
                dao.deleteItemById(newest.getId());
                dao.insert(newest);
            }
        }
    }

    @Override
    public synchronized void saveRequests(ArrayList<Request> requests) {
        RequestDao dao = AppManager.getInstance().getRoom().getRequestDao();
        for (int i = 0; requests.size() > i; i++) {
            Request newest = requests.get(i);
            Request older = dao.getItemById(newest.getId());
            if (!compareRequests(newest, older)) {
                dao.deleteById(newest.getId());
                dao.insert(newest);
            }
        }
    }

    @Override
    public synchronized void saveUsers(List<User> users) {
        UserDao dao = AppManager.getInstance().getRoom().getUserDao();
        for (int i = 0; users.size() > i; i++) {
            User newest = users.get(i);
            User older = dao.getByUid(newest.getUid());
            if (!compareUsers(newest, older)) {
                dao.deleteByUid(newest.getUid());
                dao.insert(newest);
            }
        }
    }

    @Override
    public synchronized void saveAdmin(User admin) {
        UserDao dao = AppManager.getInstance().getRoom().getUserDao();
        User older = dao.getByUid(admin.getUid());
        if (!compareUsers(admin, older)) {
            dao.deleteByUid(admin.getUid());
            dao.insert(admin);
        }
    }

    private boolean compareUsers(User newest, User older) {
        return older != null && newest != null &&
                newest.getName().equals(older.getName()) &&
                newest.getEmail().equals(older.getEmail()) &&
                newest.getDiscount().equals(older.getDiscount()) &&
                newest.getPhone().equals(older.getPhone());
    }

    private boolean compareRequests(Request newest, Request older) {
        if (newest == null)
            return false;

        if (older == null)
            return false;

        List<String> newestList = new LinkedList<>();
        List<String> olderList = new LinkedList<>();

        newestList = newest.getService();
        olderList = older.getService();

        if (newestList.size() != olderList.size()) {
            return false;
        } else {
            for (int i = 0; newestList.size() > i; i++) {
                if (olderList.indexOf(newestList.get(i)) == -1)
                    return false;
            }
        }

        return newest.getId().equals(older.getId()) &&
                newest.getTime().equals(older.getTime()) &&
                newest.getDate().equals(older.getDate()) &&
                newest.getClient().equals(older.getClient());
    }

    private boolean compareServices(Service newest, Service older) {
        return newest != null && older != null &&
                newest.getId().equals(older.getId()) &&
                newest.getPrice().equals(older.getPrice()) &&
                newest.getCost().equals(older.getCost()) &&
                newest.getDuration().equals(older.getDuration()) &&
                newest.getDescription().equals(older.getDescription());

    }
}
