package me.ibanha.bizmanager.database;

import java.util.ArrayList;
import java.util.List;

import me.ibanha.bizmanager.model.Request;
import me.ibanha.bizmanager.model.Service;
import me.ibanha.bizmanager.model.User;

public interface FirestoreSaverCallback {

    void saveServices(ArrayList<Service> items);

    void saveRequests(ArrayList<Request> items);

    void saveUsers(List<User> items);

    void saveAdmin(User admin);
}
