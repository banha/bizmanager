package me.ibanha.bizmanager.database;


import java.text.SimpleDateFormat;
import java.util.Calendar;

import me.ibanha.bizmanager.model.Request;
import me.ibanha.bizmanager.model.Service;
import me.ibanha.bizmanager.model.ServiceDao;

public class DBManager {
    private final String TAG = this.getClass().getSimpleName();

    private String id;
    private AppManager manager = AppManager.instance;
    private FireDatabase fireDB = manager.getFireDatabase();
    private AppDatabase appDB = manager.getRoom();

    public DBManager() {
    }

    public DBManager(String id) {
        this.id = id;
    }

    public void moveToConfirmTable() {
//        ItemUnconfirmedDao unconfDao = appDB.getItemUnconfDao();
//        ItemConfirmedDao confDao = appDB.getItemConfDao();
//        ItemUnconfirmed unconf = unconfDao.getItem(id);
//        ItemConfirmed conf = new ItemConfirmed();
//
//        conf.setTime(unconf.getTime());
//        conf.setPersonName(unconf.getPersonName());
//        conf.setPhone(unconf.getPhone());
//        conf.setTitle(unconf.getTitle());
//
//        confDao.insert(conf);
//        delete(unconf);
//        fireDB.moveToConfirmed(id, getDate());
    }

    public void delete(Request item) {
//        ItemUnconfirmedDao unconfDao = appDB.getItemUnconfDao();
//
//        fireDB.deleteRequestItemById(id, getDate());
//        unconfDao.delete(item);
    }

    private String getDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormatterEntry = new SimpleDateFormat("M.dd.yyyy");
        return dateFormatterEntry.format(calendar.getTime());
    }

    public void deleteServiceItem(Service item) {
        ServiceDao dao = appDB.getServiceDao();
        dao.deleteItemById(item.getId());
        fireDB.deleteServiceItemById(item.getId());
    }

    public void restoreServiceItem(Service item) {
        ServiceDao dao = appDB.getServiceDao();
        dao.insert(item);
        fireDB.insertServiceItem(item);
    }

    public void deleteUnconfirmedItem(Request item) {
//        ItemUnconfirmedDao dao = appDB.getItemUnconfDao();
//        dao.delete(item);
//        fireDB.deleteRequestItemById(item.getTime(), getDate());
    }


    public void restoreUnconfirmedItem(Request item) {
//        ItemUnconfirmedDao dao = appDB.getItemUnconfDao();
//        dao.insert(item);
//
//        TaskItem taskItem = new TaskItem();
//        taskItem.setClient(item.getPersonName());
//        taskItem.setPhone(item.getPhone());
//        taskItem.setDate(getDate());
//        taskItem.setTime(item.getTime());
//        taskItem.setServiceId(item.getTitle());
//
//        fireDB.insertUnconfItem(taskItem);
    }

    public void insertConfTask(Request item) {
//        ItemConfirmedDao confDao = appDB.getItemConfDao();
//
//        ItemConfirmed conf = new ItemConfirmed();
//        conf.setPersonName(item.getClient());
//        conf.setPhone(item.getPhone());
//        conf.setTime(item.getTime());
//        conf.setTitle(item.getServiceId());
//
//        fireDB.insertConfTask(item);
//        confDao.insert(conf);
    }

    public void insertUnconfTask(Request item) {
//        ItemUnconfirmedDao unconfDao = appDB.getItemUnconfDao();
//
//        ItemUnconfirmed unconf = new ItemUnconfirmed();
//        unconf.setPersonName(item.getClient());
//        unconf.setPhone(item.getPhone());
//        unconf.setTime(item.getTime());
//        unconf.setTitle(item.getServiceId());
//
//        fireDB.insertUnconfItem(item);
//        unconfDao.insert(unconf);
    }

    public Service getServiceItem(String id) {
        ServiceDao dao = appDB.getServiceDao();
        return dao.getById(id);
    }

    public void updateServiceItem(Service updated, Service old) {
        FireDatabase fireDB = AppManager.instance.getFireDatabase();
        ServiceDao dao = AppManager.instance.getRoom().getServiceDao();

        if (old.getId().equals(updated.getId())) {
            dao.deleteItemById(updated.getId());
            dao.insert(updated);

//            fireDB.insertServiceItem(updated);
        } else {
            dao.deleteItemById(old.getId());
            dao.insert(updated);

//            fireDB.deleteServiceItemById(old);
//            fireDB.insertServiceItem(updated);
        }
    }

    public Request getConfirmedTaskItem(String id) {

        return new Request();
    }

    public Request getUnconfirmedTaskItem(String id) {
//        ItemUnconfirmedDao unconf = appDB.getItemUnconfDao();
        return new Request();
    }

    public void updateConfTask(Request item, String date) {
//        ItemConfirmedDao confDao = appDB.getItemConfDao();
//        confDao.deleteItem(item.getTime());
//        fireDB.deleteConfirmedItem(item.getTime(), date);
//
//        ItemConfirmed conf = new ItemConfirmed();
//
//        conf.setPersonName(item.getClient());
//        conf.setPhone(item.getPhone());
//        conf.setTime(item.getTime());
//        conf.setTitle(item.getServiceId());
//
//        fireDB.insertConfTask(item);
//        if (date.equals(item.getDate()))
//            confDao.insert(conf);
    }

    public void updateUnconfTask(Request item, String date) {
//        ItemUnconfirmedDao unconfDao = appDB.getItemUnconfDao();
//        unconfDao.deleteItem(item.getTime());
//        fireDB.deleteRequestItemById(item.getTime(), date);
//
//        ItemUnconfirmed unconf = new ItemUnconfirmed();
//        unconf.setPersonName(item.getClient());
//        unconf.setPhone(item.getPhone());
//        unconf.setTime(item.getTime());
//        unconf.setTitle(item.getServiceId());
//
//        fireDB.insertUnconfItem(item);
//        if (date.equals(item.getDate()))
//            unconfDao.insert(unconf);
    }
}
