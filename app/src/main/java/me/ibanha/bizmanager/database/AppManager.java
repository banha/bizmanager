package me.ibanha.bizmanager.database;

import android.app.Application;
import android.arch.persistence.room.Room;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AppManager extends Application {
    //TODO make the instance var private!
    public static AppManager instance;
    private final String TAG = this.getClass().getSimpleName();
    private AppDatabase room;
    private FireDatabase firestore;

    public static AppManager getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        room = Room.databaseBuilder(this, AppDatabase.class, "room")
                .allowMainThreadQueries()
                .build();
        firestore = FireDatabase.getInstance();
    }

    public AppDatabase getRoom() {
        return room;
    }

    public FireDatabase getFireDatabase() {
        return firestore;
    }

    public String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormatterEntry = new SimpleDateFormat("M.dd.yyyy hh:mm");
        return dateFormatterEntry.format(calendar.getTime());
    }
}
