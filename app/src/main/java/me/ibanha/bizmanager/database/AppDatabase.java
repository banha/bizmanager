package me.ibanha.bizmanager.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import me.ibanha.bizmanager.model.Request;
import me.ibanha.bizmanager.model.RequestDao;
import me.ibanha.bizmanager.model.Service;
import me.ibanha.bizmanager.model.ServiceDao;
import me.ibanha.bizmanager.model.User;
import me.ibanha.bizmanager.model.UserDao;

@Database(entities = {
        User.class,
        Service.class,
        Request.class
}, version = 1)

public abstract class AppDatabase extends RoomDatabase {

    public abstract UserDao getUserDao();

    public abstract ServiceDao getServiceDao();

    public abstract RequestDao getRequestDao();
}
