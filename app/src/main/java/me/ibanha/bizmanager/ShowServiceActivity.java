package me.ibanha.bizmanager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import me.ibanha.bizmanager.database.AppManager;
import me.ibanha.bizmanager.database.DBManager;
import me.ibanha.bizmanager.database.FireDatabase;
import me.ibanha.bizmanager.fragment.picker.TimePickerFragment;
import me.ibanha.bizmanager.model.Service;

public class ShowServiceActivity extends AppCompatActivity implements TimePickerFragment.TimePickedListener {

    private ScrollView showLayout;
    private ScrollView editLayout;
    private Menu menu;
    private DBManager manager;

    private Service old;
    private Service updated;

    private TextView title;
    private TextView price;
    private TextView cost;
    private TextView duration;
    private TextView description;

    private EditText editTitle;
    private EditText editPrice;
    private EditText editCost;
    private EditText editDuration;
    private EditText editDescription;

    private TextView errTitle;
    private TextView errPrice;
    private TextView errCost;
    private TextView errDuration;
    private TextView errDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_service);

        showLayout = findViewById(R.id.show_service_item_layout);
        editLayout = findViewById(R.id.edit_service_item_layout);

        title = findViewById(R.id.txt_title_service_item);
        price = findViewById(R.id.txt_price_service_item);
        duration = findViewById(R.id.txt_duration_service_item);
        description = findViewById(R.id.txt_description_service_item);

        updated = new Service();
        manager = new DBManager();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        old = manager.getServiceItem(intent.getStringExtra("id"));

        title.setText(old.getId());
        price.setText(String.valueOf(old.getPrice()));
        duration.setText(old.getDuration());
        description.setText(old.getDescription());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        FireDatabase fire = AppManager.getInstance().getFireDatabase();
        if (fire.isAdmin()) {
            getMenuInflater().inflate(R.menu.menu_show_service_item, menu);
        }
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_edit:
                setTitle(R.string.title_activity_edit_service_item);
                showLayout.setVisibility(View.GONE);
                editLayout.setVisibility(View.VISIBLE);
                item.setVisible(false);
                menu.findItem(R.id.btn_update).setVisible(true);
                initEditLayout();
                setEditTextData();
                return true;
            case R.id.btn_update:
                send();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onTimePicked(String time) {
        editDuration.setText(time);
    }

    public void showTimePicker(View view) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    private void initEditLayout() {
        editTitle = findViewById(R.id.edit_service_title);
        editPrice = findViewById(R.id.edit_service_price);
        editCost = findViewById(R.id.edit_service_cost);
        editDuration = findViewById(R.id.edit_service_duration);
        editDescription = findViewById(R.id.edit_service_description);

        errTitle = findViewById(R.id.err_title);
        errPrice = findViewById(R.id.err_price);
        errCost = findViewById(R.id.err_cost);
        errDuration = findViewById(R.id.err_duration);
        errDescription = findViewById(R.id.err_description);
    }

    private void setEditTextData() {
        editTitle.setText(old.getId());
        editPrice.setText(String.valueOf(old.getPrice()));
        editCost.setText(String.valueOf(old.getCost()));
        editDuration.setText(old.getDuration());
        editDescription.setText(old.getDescription());
    }

    private void send() {
        clearViews();

        if (!editTitle.getText().toString().isEmpty()) {
            updated.setId(editTitle.getText().toString());
        } else {
            errTitle.setText(R.string.err_title);
            return;
        }

        if (!editCost.getText().toString().isEmpty()) {
            updated.setCost(Long.valueOf(editCost.getText().toString()));
        } else {
            errCost.setText(R.string.err_cost);
            return;
        }

        if (!editPrice.getText().toString().isEmpty()) {
            updated.setPrice(Long.valueOf(editPrice.getText().toString()));
        } else {
            errPrice.setText(R.string.err_price);
            return;
        }

        if (!editDuration.getText().toString().isEmpty()) {
            updated.setDuration(editDuration.getText().toString());
        } else {
            errDuration.setText(R.string.err_duration);
            return;
        }

        if (!editDescription.getText().toString().isEmpty()) {
            updated.setDescription(editDescription.getText().toString());
        } else {
            errDescription.setText(R.string.err_description);
            return;
        }

        Sender sender = new Sender();
        sender.execute();
    }

    private void clearViews() {
        errTitle.setText("");
        errPrice.setText("");
        errDuration.setText("");
        errDescription.setText("");
    }

    @SuppressLint("StaticFieldLeak")
    private class Sender extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            manager.updateServiceItem(updated, old);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(getApplicationContext(), "Successfully!", Toast.LENGTH_SHORT).show();
            setResult(RESULT_OK);
            finish();
        }
    }
}
