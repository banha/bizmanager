package me.ibanha.bizmanager;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.ibanha.bizmanager.database.AppManager;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private final String TAG = this.getClass().getSimpleName();
    private final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private Pattern pattern;

    private EditText emailView;
    private EditText passwordView;
    private TextView emailMsg;
    private TextView passwordMsg;
    private TextView commonMsg;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initWidgets();

        pattern = Pattern.compile(EMAIL_PATTERN);
        auth = FirebaseAuth.getInstance();
    }

    @Override
    public void onClick(View view) {
        String email = this.emailView.getText().toString();
        String password = this.passwordView.getText().toString();
        passwordMsg.setText("");
        emailMsg.setText("");
        commonMsg.setText("");

        switch (view.getId()) {
            case R.id.sing_in_btn:
                if (areValuesValid(email, password))
                    singIn(email, password);
                break;
            case R.id.sign_up_btn:
                if (areValuesValid(email, password))
                    signUp(email, password);
                break;
        }
    }


    @Override
    public boolean onNavigateUp() {
        finish();
        return true;
    }

    private void initWidgets() {
        emailView = findViewById(R.id.email_in);
        passwordView = findViewById(R.id.password_in);
        emailMsg = findViewById(R.id.msg_email);
        passwordMsg = findViewById(R.id.msg_password);
        commonMsg = findViewById(R.id.msg_common);
    }

    private void singIn(String email, String password) {
        Toast.makeText(this, "Checking!", Toast.LENGTH_LONG).show();
        auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "Successfully!");
                            AppManager.instance.getFireDatabase()
                                    .updateUser(auth.getCurrentUser());

                            Toast.makeText(getApplicationContext(), "Sign In Successfully! :)",
                                    Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "Failed!", e);
                        commonMsg.setText(e.getMessage());
                    }
                });
    }

    private void signUp(String email, String paswd) {
        Toast.makeText(this, "Checking!", Toast.LENGTH_LONG).show();
        auth.createUserWithEmailAndPassword(email, paswd)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.i(TAG, "Sign Up!");
                            AppManager.instance.getFireDatabase()
                                    .updateUser(auth.getCurrentUser());
                            Toast.makeText(getApplicationContext(), "Sign Up Successfully!",
                                    Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), AccountInfoActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "Failed!", e);
                        commonMsg.setText(e.getMessage());
                    }
                });
    }

    private boolean areValuesValid(String email, String password) {
        if (!email.equals("") && isValidEmail(email)) {
            if (!password.equals("") && password.length() >= 6) {
                return true;
            } else {
                passwordMsg.setText(R.string.password_error);
                return false;
            }
        } else {
            emailMsg.setText(R.string.email_error);
            return false;
        }
    }

    private boolean isValidEmail(String email) {
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
