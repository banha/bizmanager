package me.ibanha.bizmanager;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import me.ibanha.bizmanager.database.AppManager;
import me.ibanha.bizmanager.database.FireDatabase;
import me.ibanha.bizmanager.fragment.picker.DatePickerFragment;
import me.ibanha.bizmanager.fragment.picker.TimePickerFragment;
import me.ibanha.bizmanager.helper.EmailTemplate;
import me.ibanha.bizmanager.helper.Helper;
import me.ibanha.bizmanager.model.Request;
import me.ibanha.bizmanager.model.RequestDao;
import me.ibanha.bizmanager.model.Service;
import me.ibanha.bizmanager.model.ServiceDao;
import me.ibanha.bizmanager.model.User;
import me.ibanha.bizmanager.model.UserDao;
import me.ibanha.bizmanager.model.control.Constructor;
import me.ibanha.bizmanager.model.control.Model;

public class CreateRequestActivity extends AppCompatActivity
        implements DatePickerFragment.DatePickedListener, TimePickerFragment.TimePickedListener {

    private final String TAG = this.getClass().getSimpleName();
    private final String CURRENCY = " грн";
    private AlertDialog.Builder builder;

    private boolean type;

    private EditText eName;
    private EditText ePhone;
    private EditText eDate;
    private EditText eTime;
    private TextView chosen;
    private TextView discountVal;
    private TextView servicesPrice;
    private TextView discountAmount;
    private TextView priceWithDiscount;
    private TextView commonTime;

    private TextView vErrorName;
    private TextView vErrorPhone;
    private TextView vErrorDate;
    private TextView vErrorTime;
    private TextView vErrorChooser;

    private ArrayList<String> items;
    private ArrayList<Service> services;
    private ArrayList<String> checkedItems;
    private User user;
    private Helper helper = new Helper();
    private long choosenServicesPrice;
    private String id;
    private Model model;
    private Map<String, Object> params = new HashMap<>();
    private int mode;

    String[] list;
    boolean[] checked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);

        eName = findViewById(R.id.edit_txt_name_unconfirmed);
        ePhone = findViewById(R.id.edit_txt_phone_unconfirmed);
        eDate = findViewById(R.id.edit_txt_date_picker);
        eTime = findViewById(R.id.edit_txt_time_picker);
        chosen = findViewById(R.id.choose_text_view);
        discountVal = findViewById(R.id.tv_discount_value);
        discountAmount = findViewById(R.id.tv_discount_amount);
        servicesPrice = findViewById(R.id.tv_summary);
        priceWithDiscount = findViewById(R.id.tv_price_with_discount);
        commonTime = findViewById(R.id.tv_common_time);

        eDate.setInputType(InputType.TYPE_NULL);
        eTime.setInputType(InputType.TYPE_NULL);

        vErrorName = findViewById(R.id.err_name_unconfirmed);
        vErrorPhone = findViewById(R.id.err_phone_unconfirmed);
        vErrorDate = findViewById(R.id.err_date);
        vErrorTime = findViewById(R.id.err_time);
        vErrorChooser = findViewById(R.id.err_service_chooser);

        Price task = new Price();
        task.execute();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        mode = intent.getIntExtra("mode", 0);

        id = intent.getStringExtra("id");
        type = intent.getBooleanExtra("type", false);

        if (mode == 3) {
            params.put("id", intent.getStringExtra("id"));
            params.put("type", intent.getBooleanExtra("type", false));
            params.put("mode", intent.getIntExtra("mode", 0));
            DataInit dataInit = new DataInit();

            try {
                model = dataInit.execute(params).get();
                setEditTextData();
            } catch (InterruptedException | ExecutionException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_new_universal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_save:
                send();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setEditTextData() {
        checkedItems = (ArrayList<String>) model.getServiceIds();
        eName.setText(model.getName());
        ePhone.setText(model.getPhone());
        eDate.setText(model.getDate());
        eTime.setText(model.getTime());
        discountVal.setText(String.valueOf(model.getDiscount()) + "%");
        discountAmount.setText(String.valueOf(
                model.getPricesWithoutDiscount() - model.getPricesWithDiscount() + " грн"));
        servicesPrice.setText(String.valueOf((int) model.getPricesWithoutDiscount()) + " грн");
        priceWithDiscount.setText(String.valueOf((int) model.getPricesWithDiscount()) + " грн");
        commonTime.setText(model.getDuration() + " год");
    }

    @Override
    public boolean onNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onTimePicked(String time) {
        eTime.setText(time);
    }

    @Override
    public void onDatePicked(String date) {
        eDate.setText(date);
    }

    public void showDatePicker(View view) {
        DialogFragment datePicker = new DatePickerFragment();
        datePicker.show(getSupportFragmentManager(), "timePicker");
    }

    public void showTimePicker(View view) {
        DialogFragment timePicker = new TimePickerFragment();
        timePicker.show(getSupportFragmentManager(), "timePicker");
    }

    public void onClick(View view) {
        showDialog(1);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        builder = new AlertDialog.Builder(this);
        builder.setTitle("Виберіть послуги")
                .setCancelable(false)
                .setMultiChoiceItems(list, checked,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which, boolean isChecked) {
                                checked[which] = isChecked;
                            }
                        })

                .setPositiveButton("Ок",
                        new DialogInterface.OnClickListener() {
                            @SuppressLint("SetTextI18n")
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                StringBuilder state = new StringBuilder();
                                ArrayList<String> times = new ArrayList<>();
                                checkedItems = new ArrayList<>();
                                choosenServicesPrice = 0;
                                for (int i = 0; i < list.length; i++) {
                                    if (checked[i]) {
                                        int position = helper.findValuePosition(list[i], services);
                                        Service service = services.get(position);
                                        times.add(service.getDuration());
                                        long price = service.getPrice();
                                        choosenServicesPrice = choosenServicesPrice + price;
                                        state.append("" + list[i] + "  -  " + price + CURRENCY + "\n");
                                        checkedItems.add(list[i]);
                                    }
                                }
                                setValues(state.toString(), times);
                            }
                        })

                .setNegativeButton("Скасувати",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.cancel();

                            }
                        });
        return builder.create();
    }

    @SuppressLint("SetTextI18n")
    private void setValues(String state, ArrayList<String> times) {

        chosen.setText(state);
        discountAmount.setText(
                String.valueOf(helper.calcDiscount(user.getDiscount(),
                        (int) choosenServicesPrice)) + CURRENCY);

        priceWithDiscount.setText(String.valueOf(
                choosenServicesPrice - helper.calcDiscount(
                        user.getDiscount(), (int) choosenServicesPrice)) + CURRENCY);

        servicesPrice.setText(String.valueOf(choosenServicesPrice) + CURRENCY);
        commonTime.setText(helper.calculateSumOfHours(times) + " год");
    }

    private void send() {
        clearViews();
        Request request = new Request();

        if (null != checkedItems && !checkedItems.isEmpty()) {
            request.setService(checkedItems);
        } else {
            vErrorChooser.setText(R.string.err_chooser);
            return;
        }

        if (!eName.getText().toString().isEmpty()) {
            request.setClient(user.getUid());
        } else {
            vErrorName.setText(R.string.err_name);
            return;
        }

        if (ePhone.getText().toString().isEmpty()) {
            vErrorPhone.setText(R.string.err_phone);
            return;
        }

        if (!eDate.getText().toString().isEmpty()) {
            request.setDate(eDate.getText().toString());
        } else {
            vErrorDate.setText(R.string.err_date);
            return;
        }

        if (!eTime.getText().toString().isEmpty()) {
            request.setTime(eTime.getText().toString());
        } else {
            vErrorTime.setText(R.string.err_time);
            return;
        }

        request.setId(request.getDate() + " " + request.getTime());

        Sender sender = new Sender();
        sender.execute(request);
    }

    private void clearViews() {
        vErrorName.setText("");
        vErrorPhone.setText("");
        vErrorDate.setText("");
        vErrorTime.setText("");
    }

    @SuppressLint("StaticFieldLeak")
    private class Price extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            ServiceDao serviceDao = AppManager.instance.getRoom().getServiceDao();
            UserDao userDao = AppManager.getInstance().getRoom().getUserDao();

            items = new ArrayList<>();
            user = userDao.getByUid(AppManager.getInstance().getFireDatabase().getUserUid());
            services = (ArrayList<Service>) serviceDao.getItems();

            ArrayList<Service> prices = (ArrayList<Service>) serviceDao.getItems();
            for (int i = 0; i < prices.size(); i++) {
                items.add(prices.get(i).getId());
            }
            return null;
        }

        @SuppressLint("SetTextI18n")
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            list = items.toArray(new String[0]);
            checked = new boolean[list.length];

            StringBuilder state = new StringBuilder();
            if (mode == 3) {
                ArrayList<Service> serviceList = (ArrayList<Service>) model.getServices();

                for (int serviceCounter = 0; serviceList.size() > serviceCounter; serviceCounter++) {
                    for (int itemCounter = 0; list.length > itemCounter; itemCounter++) {
                        if (serviceList.get(serviceCounter).getId().equals(list[itemCounter])) {
                            checked[itemCounter] = true;
                            state.append("" + list[itemCounter] + "  -  " + serviceList.get(serviceCounter).getPrice() + CURRENCY + "\n");
                        }
                    }
                }
            }

            if (mode != 3) {
                eName.setText(user.getName());
                ePhone.setText(user.getPhone());
                discountVal.setText("- " + user.getDiscount() + "%");
            }

            chosen.setText(state.toString());
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class Sender extends AsyncTask<Request, Void, Void> {

        @Override
        protected Void doInBackground(Request... requests) {
            EmailTemplate email = new EmailTemplate();
            RequestDao dao = AppManager.getInstance().getRoom().getRequestDao();
            FireDatabase firestore = AppManager.getInstance().getFireDatabase();
            Request request = requests[0];
            request.setApproved(type);

            try {
                dao.deleteById(request.getId());
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (mode == 3) {
                String id = model.getDate() + " " + model.getTime();
                request.setClient(model.getUid());
                firestore.deleteRequestItemById(id);
            }

            firestore.insertRequestItem(request);
            dao.insert(request);

            if (mode == 3)
                email.composeEmailByEditrequestTemplate(request);
            else
                email.composeEmailByNewRequestTemplate(request);

            email.send();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(getApplicationContext(), "Successfully!", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class DataInit extends AsyncTask<Map, Void, Model> {

        @Override
        protected Model doInBackground(Map... maps) {
            RequestDao dao = AppManager.getInstance().getRoom().getRequestDao();

            String id = (String) maps[0].get("id");
            Boolean type = (Boolean) maps[0].get("type");
            Constructor constructor = new Constructor();
            constructor.init(id, type);
            return constructor.getModel();
        }

        @SuppressLint("SetTextI18n")
        @Override
        protected void onPostExecute(Model model) {
            super.onPostExecute(model);

            setTitle("Редагування");
        }
    }
}
