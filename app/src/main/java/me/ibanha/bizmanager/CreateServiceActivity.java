package me.ibanha.bizmanager;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import me.ibanha.bizmanager.database.AppManager;
import me.ibanha.bizmanager.fragment.picker.TimePickerFragment;
import me.ibanha.bizmanager.model.Service;
import me.ibanha.bizmanager.model.ServiceDao;

public class CreateServiceActivity extends AppCompatActivity implements TimePickerFragment.TimePickedListener {
    private final String TAG = this.getClass().getSimpleName();

    private EditText name;
    private EditText price;
    Service service = new Service();
    private EditText duration;
    private EditText description;

    private TextView errTitle;
    private TextView errPrice;
    private EditText cost;
    private TextView errDuration;
    private TextView errDescription;
    private TextView errCost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_service);

        name = findViewById(R.id.edit_service_title);
        price = findViewById(R.id.edit_service_price);
        cost = findViewById(R.id.edit_service_cost);
        duration = findViewById(R.id.edit_service_duration);
        description = findViewById(R.id.edit_service_description);

        duration.setInputType(InputType.TYPE_NULL);

        errTitle = findViewById(R.id.err_title);
        errPrice = findViewById(R.id.err_price);
        errCost = findViewById(R.id.err_cost);
        errDuration = findViewById(R.id.err_duration);
        errDescription = findViewById(R.id.err_description);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_new_universal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_save:
                send();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigateUp() {
        setResult(RESULT_CANCELED);
        finish();
        return true;
    }

    public void showTimePicker(View view) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    private void send() {
        clearViews();

        if (!name.getText().toString().isEmpty()) {
            service.setId(name.getText().toString());
        } else {
            errTitle.setText(R.string.err_title);
            return;
        }

        if (!price.getText().toString().isEmpty()) {
            service.setPrice(Long.valueOf(price.getText().toString()));
        } else {
            errPrice.setText(R.string.err_price);
            return;
        }

        if (!cost.getText().toString().isEmpty()) {
            service.setCost(Long.valueOf(cost.getText().toString()));
        } else {
            errCost.setText(R.string.err_cost);
            return;
        }

        if (!duration.getText().toString().isEmpty()) {
            service.setDuration(duration.getText().toString());
        } else {
            errDuration.setText(R.string.err_duration);
            return;
        }

        if (!description.getText().toString().isEmpty()) {
            service.setDescription(description.getText().toString());
        } else {
            errDescription.setText(R.string.err_description);
            return;
        }

        Sender sender = new Sender();
        sender.execute();
    }

    private void clearViews() {
        errTitle.setText("");
        errPrice.setText("");
        errCost.setText("");
        errDuration.setText("");
        errDescription.setText("");
    }

    @Override
    public void onTimePicked(String time) {
        duration.setText(time);
    }

    @SuppressLint("StaticFieldLeak")
    private class Sender extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            AppManager.instance.getFireDatabase().insertServiceItem(service);
            ServiceDao dao = AppManager.instance.getRoom().getServiceDao();
            dao.insert(service);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(getApplicationContext(), "Successfully!", Toast.LENGTH_SHORT).show();
            setResult(RESULT_OK);
            finish();
        }
    }
}
