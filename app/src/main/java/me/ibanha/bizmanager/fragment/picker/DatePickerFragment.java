package me.ibanha.bizmanager.fragment.picker;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.DatePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private final String TAG = this.getClass().getSimpleName();

    private SimpleDateFormat dateFormatterEntry = new SimpleDateFormat("M.dd.yyyy");
    private Calendar calendar;
    private int year, month, day;
    private DatePickedListener listener;

    public static interface DatePickedListener {
        void onDatePicked(String date);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        listener = (DatePickedListener) getActivity();

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        calendar.set(year, month, dayOfMonth, 0, 0);
        String formattedDate = dateFormatterEntry.format(calendar.getTime());
        Log.i(TAG, formattedDate);
        listener.onDatePicked(formattedDate);
    }
}
