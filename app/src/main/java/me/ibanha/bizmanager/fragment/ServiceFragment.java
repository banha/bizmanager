package me.ibanha.bizmanager.fragment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.ArrayList;

import me.ibanha.bizmanager.CreateServiceActivity;
import me.ibanha.bizmanager.R;
import me.ibanha.bizmanager.ShowServiceActivity;
import me.ibanha.bizmanager.adapter.ServiceAdapter;
import me.ibanha.bizmanager.database.AppManager;
import me.ibanha.bizmanager.database.DBManager;
import me.ibanha.bizmanager.database.FireDatabase;
import me.ibanha.bizmanager.helper.ItemClickSupport;
import me.ibanha.bizmanager.helper.SwipeServices;
import me.ibanha.bizmanager.model.Service;
import me.ibanha.bizmanager.model.ServiceDao;

public class ServiceFragment extends Fragment implements SwipeServices.RecyclerItemTouchHelperListener {
    private final String TAG = this.getClass().getSimpleName();

    private ArrayList<Service> items;
    private ServiceAdapter adapter;
    private RecyclerView recyclerView;
    private FrameLayout containerLayout;
    private FloatingActionButton fab;

    public ServiceFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_service, container, false);
        getActivity().setTitle(getString(R.string.title_services));

        containerLayout = view.findViewById(R.id.layout_snack);
        recyclerView = view.findViewById(R.id.service_list);
        adapter = new ServiceAdapter(R.layout.item_service, items);
        fab = view.findViewById(R.id.fab_service);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        FireDatabase fire = AppManager.getInstance().getFireDatabase();
        initItems();
        attachOnClickListener();

        if (fire.isAdmin()) {
            attachOnSwipeListener();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) {
                        fab.hide();
                    } else {
                        fab.show();
                    }
                    super.onScrolled(recyclerView, dx, dy);
                }
            });
        } else {
            fab.hide();
        }
        GetData task = new GetData();
        task.execute();
    }

    private void initItems() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getBaseContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity().getBaseContext(),
                DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);
    }

    private void attachOnClickListener() {
        ItemClickSupport.addTo(recyclerView)
                .setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView r, int p, View v) {
                        Intent intent = new Intent(getActivity().getBaseContext(), ShowServiceActivity.class);
                        intent.putExtra("id", items.get(p).getId());
                        startActivity(intent);
                    }
                });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newService = new Intent(getActivity().getBaseContext(), CreateServiceActivity.class);
                startActivityForResult(newService, 1);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, requestCode + " " + resultCode);
    }

    private void attachOnSwipeListener() {
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new SwipeServices(
                0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        Log.d(TAG, "onSwiped!");
        if (viewHolder instanceof ServiceAdapter.ViewHolder) {
            final DBManager manager = new DBManager();

            String name = items.get(viewHolder.getAdapterPosition()).getId();
            final Service deletedItem = items.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            adapter.deleteItem(viewHolder.getAdapterPosition());
            manager.deleteServiceItem(deletedItem);

            Snackbar snackbar = Snackbar.make(containerLayout, name +
                    " видалено!", Snackbar.LENGTH_LONG);

            snackbar.setAction("Відновити", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapter.restoreItem(deletedItem, deletedIndex);
                    manager.restoreServiceItem(deletedItem);
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class GetData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            adapter.updateDataset(items);
            adapter.notifyDataSetChanged();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ServiceDao service = AppManager.instance.getRoom().getServiceDao();
            items = (ArrayList<Service>) service.getItems();

            return null;
        }
    }
}
