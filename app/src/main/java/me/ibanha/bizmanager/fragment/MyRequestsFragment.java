package me.ibanha.bizmanager.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Objects;

import me.ibanha.bizmanager.CreateRequestActivity;
import me.ibanha.bizmanager.R;
import me.ibanha.bizmanager.adapter.ListAdapter;
import me.ibanha.bizmanager.database.AppManager;
import me.ibanha.bizmanager.database.FireDatabase;
import me.ibanha.bizmanager.model.control.Constructor;
import me.ibanha.bizmanager.model.control.Model;


public class MyRequestsFragment extends Fragment {

    private final String INTENT_TYPE = "type";
    private final boolean REQUEST_TYPE = false;

    private ArrayList<Model> items;
    private ListAdapter adapter;

    public MyRequestsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_requests, container, false);
        final FloatingActionButton fab = view.findViewById(R.id.fab_my_requests_list);

        adapter = new ListAdapter(getContext(), REQUEST_TYPE, R.layout.item_request_for_user, items, true);

        RecyclerView recyclerView = view.findViewById(R.id.my_requests_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(
                Objects.requireNonNull(getActivity()).getBaseContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    fab.hide();
                } else {
                    fab.show();
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getBaseContext(), CreateRequestActivity.class);
                intent.putExtra(INTENT_TYPE, REQUEST_TYPE);
                intent.putExtra("mode", 1);
                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        InitView task = new InitView();
        task.execute();
    }

    @SuppressLint("StaticFieldLeak")
    private class InitView extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            FireDatabase fireDatabase = AppManager.getInstance().getFireDatabase();
            Constructor constructor = new Constructor(fireDatabase.getUserUid());
            items = constructor.getModels();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            adapter.notifyDataSetChanged();
            adapter.updateItems(items);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }

}
