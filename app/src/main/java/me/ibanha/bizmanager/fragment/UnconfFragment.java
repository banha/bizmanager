package me.ibanha.bizmanager.fragment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import me.ibanha.bizmanager.CreateRequestActivity;
import me.ibanha.bizmanager.R;
import me.ibanha.bizmanager.adapter.ListAdapter;
import me.ibanha.bizmanager.database.AppManager;
import me.ibanha.bizmanager.model.control.Constructor;
import me.ibanha.bizmanager.model.control.Model;

public class UnconfFragment extends Fragment {


    private final String INTENT_TYPE = "type";
    private final boolean REQUEST_TYPE = false;
    private String date = AppManager.getInstance().getCurrentDate();

    private ArrayList<Model> items;
    private ListAdapter adapter;
    private Spinner datePicker;
    private ArrayList<String> dates = new ArrayList<>();

    public UnconfFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.title_unconfirmed_task));
        View view = inflater.inflate(R.layout.fragment_unconfirmed, container, false);
        final FloatingActionButton fab = view.findViewById(R.id.fab_unconfirmed_task);

        adapter = new ListAdapter(getContext(), REQUEST_TYPE, R.layout.item_request_for_admin, items);
        datePicker = view.findViewById(R.id.unconf_date_picker);

        RecyclerView recyclerView = view.findViewById(R.id.unconfirmed_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getBaseContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    fab.hide();
                } else {
                    fab.show();
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getBaseContext(), CreateRequestActivity.class);
                intent.putExtra(INTENT_TYPE, REQUEST_TYPE);
                intent.putExtra("mode", 1);
                startActivity(intent);
            }
        });

        datePicker.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {

                date = dates.get(selectedItemPosition);
                InitView task = new InitView();
                task.execute();
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }

        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        InitDataForPicker task = new InitDataForPicker();
        task.execute();
    }

    private String getDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormatterEntry = new SimpleDateFormat("M.dd.yyyy");
        return dateFormatterEntry.format(calendar.getTime());
    }


    @SuppressLint("StaticFieldLeak")
    private class InitView extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            Constructor constructor = new Constructor(date, REQUEST_TYPE);
            items = constructor.getModels();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            adapter.notifyDataSetChanged();
            adapter.updateItems(items);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class InitDataForPicker extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            List<String> fullList = AppManager.getInstance().getRoom().getRequestDao().getItemsDate(REQUEST_TYPE);
            for (int i = 0; fullList.size() > i; i++) {
                if (dates.indexOf(fullList.get(i)) == -1) {
                    dates.add(fullList.get(i));
                }
            }
            Collections.sort(dates);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(
                    getActivity().getBaseContext(), R.layout.item_spinner, dates
            );

            datePicker.setAdapter(spinnerAdapter);
            datePicker.setSelection(dates.indexOf(date));
        }
    }
}
