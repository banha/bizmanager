package me.ibanha.bizmanager.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import me.ibanha.bizmanager.CreateRequestActivity;
import me.ibanha.bizmanager.R;
import me.ibanha.bizmanager.ShowRequestActivity;
import me.ibanha.bizmanager.database.AppManager;
import me.ibanha.bizmanager.database.FireDatabase;
import me.ibanha.bizmanager.helper.EmailTemplate;
import me.ibanha.bizmanager.model.Request;
import me.ibanha.bizmanager.model.RequestDao;
import me.ibanha.bizmanager.model.control.Model;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private boolean type;
    private int layoutId;
    private ArrayList<Model> items;
    private Context context;
    private boolean withStatus;

    public ListAdapter(Context context, boolean type, int layoutId, ArrayList<Model> items) {
        this.layoutId = layoutId;
        this.items = items;
        this.context = context;
        this.type = type;
    }

    public ListAdapter(Context context, boolean type, int layoutId, ArrayList<Model> items, boolean withStatus) {
        this.layoutId = layoutId;
        this.items = items;
        this.context = context;
        this.type = type;
        this.withStatus = withStatus;
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int ViewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        return new ViewHolder(view);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @SuppressLint({"SetTextI18n", "ResourceAsColor"})
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") final int p) {
        holder.title.setText((items.get(p).getBuildedServicesIds()));
        holder.name.setText(items.get(p).getName());
        holder.phone.setText(items.get(p).getPhone());
        holder.time.setText(items.get(p).getDate() + " " + items.get(p).getTime());
        holder.price.setText(String.valueOf((items.get(p).getPricesWithDiscount())) + " грн");

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCard(p);
            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEdit(p);
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDelete(p);
            }
        });

        if (withStatus) {
            if (items.get(p).isApproved())
                holder.card.setBackgroundResource(R.color.card_confirmed);
            else
                holder.card.setBackgroundResource(R.color.card_unconfirmed);
        }

        try {
            holder.confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onConfirm(p);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void restoreItem(Model items, int position) {
        this.items.add(position, items);
        notifyItemInserted(position);
    }

    public void updateItem(int position) {
        notifyItemChanged(position);
    }

    public void deleteItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    public void updateItems(ArrayList<Model> items) {
        this.items = items;
    }

    private void onCard(int p) {
        Intent intent = new Intent(context, ShowRequestActivity.class);
        intent.putExtra("id", items.get(p).getDate() + " " + items.get(p).getTime());
        intent.putExtra("type", type);
        intent.putExtra("action", 0);
        context.startActivity(intent);
    }

    private void onConfirm(final int p) {
        final Model backup = items.get(p);
        items.remove(p);
        notifyDataSetChanged();
        if (type)
            Toast.makeText(context, "Заявку виконано! ", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(context, "Заявку прийнято! ", Toast.LENGTH_SHORT).show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                EmailTemplate email = new EmailTemplate();
                FireDatabase firebase = AppManager.getInstance().getFireDatabase();
                RequestDao dao = AppManager.getInstance().getRoom().getRequestDao();
                String id = backup.getDate() + " " + backup.getTime();

                if (type) {
                    dao.deleteById(id);
                    firebase.deleteRequestItemById(id);
                    email.composeEmailByDoneTemplate(backup);
                    email.send();
                } else {
                    Request request = dao.getItemById(id);
                    request.setApproved(true);
                    dao.deleteById(id);
                    dao.insert(request);
                    firebase.insertRequestItem(request);
                    email.composeEmailByConfirmedTemplate(backup);
                    email.send();
                }
            }
        }).start();
    }

    private void onEdit(int p) {
        Intent intent = new Intent(context, CreateRequestActivity.class);
        intent.putExtra("id", items.get(p).getDate() + " " + items.get(p).getTime());
        intent.putExtra("type", type);
        intent.putExtra("mode", 3);
        context.startActivity(intent);
    }

    private void onDelete(int p) {
        final Model backup = items.get(p);
        items.remove(p);
        notifyDataSetChanged();
        Toast.makeText(context, "Заявку видалено! ", Toast.LENGTH_SHORT).show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                FireDatabase firebase = AppManager.getInstance().getFireDatabase();
                RequestDao dao = AppManager.getInstance().getRoom().getRequestDao();
                String id = backup.getDate() + " " + backup.getTime();

                dao.deleteById(id);
                firebase.deleteRequestItemById(id);
            }
        }).start();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private CardView card;
        private ConstraintLayout userCard;
        private TextView title, name, phone, time, price;
        private ImageView confirm, edit, delete;

        ViewHolder(View view) {
            super(view);
            card = view.findViewById(R.id.item_unconfirmed_first_layout);
            title = view.findViewById(R.id.title);
            name = view.findViewById(R.id.txt_person_name);
            phone = view.findViewById(R.id.txt_phone);
            time = view.findViewById(R.id.txt_time);
            price = view.findViewById(R.id.txt_price);
            edit = view.findViewById(R.id.btn_edit);
            delete = view.findViewById(R.id.btn_delete);
            try {
                confirm = view.findViewById(R.id.btn_confirm);
                userCard = view.findViewById(R.id.user_card);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}