package me.ibanha.bizmanager.adapter;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import me.ibanha.bizmanager.R;
import me.ibanha.bizmanager.model.Service;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder> {

    private int listItemLayout;
    private ArrayList<Service> item;

    public ServiceAdapter(int layoutId, ArrayList<Service> item) {
        listItemLayout = layoutId;
        this.item = item;
    }

    @Override
    public int getItemCount() {
        return item == null ? 0 : item.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int ViewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(listItemLayout, parent, false);
        return new ViewHolder(view);
    }

    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        TextView name = holder.name;
        TextView price = holder.price;
        TextView duration = holder.duration;
        TextView description = holder.description;

        name.setText(this.item.get(position).getId());
        price.setText(String.valueOf(this.item.get(position).getPrice()));
        duration.setText(this.item.get(position).getDuration());
        description.setText(this.item.get(position).getDescription());
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ConstraintLayout firstLayout, secondLayout;
        private ImageView imgDelete, imgEdit;
        private TextView name, price, duration, description, txtEdit, txtDelete;

        ViewHolder(View v) {
            super(v);
            name = v.findViewById(R.id.title);
            price = v.findViewById(R.id.price);
            duration = v.findViewById(R.id.list_duration);
            description = v.findViewById(R.id.list_unconfirmed_description);

            firstLayout = (ConstraintLayout) v.findViewById(R.id.item_unconfirmed_first_layout);
            secondLayout = (ConstraintLayout) v.findViewById(R.id.item_unconfirmed_second_layout);

            imgDelete = (ImageView) v.findViewById(R.id.img_unconfirmed_list_delete);
            txtDelete = (TextView) v.findViewById(R.id.txt_delete);
            imgEdit = (ImageView) v.findViewById(R.id.img_unconfirmed_list_edit);
//            txtEdit = (TextView) v.findViewById(R.id.txt_edit);
        }

        public ConstraintLayout getBackground() {
            return secondLayout;
        }

        public ConstraintLayout getForeground() {
            return firstLayout;
        }
    }

    public void updateItem(int position) {
        notifyItemChanged(position);
    }

    public void deleteItem(int position) {
        item.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(Service item, int position) {
        this.item.add(position, item);
        notifyItemInserted(position);
    }

    public void updateDataset(ArrayList<Service> items) {
        this.item = items;
    }
}