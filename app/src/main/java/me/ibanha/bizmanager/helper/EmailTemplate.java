package me.ibanha.bizmanager.helper;

import android.util.Log;

import me.ibanha.bizmanager.database.AppManager;
import me.ibanha.bizmanager.database.FireDatabase;
import me.ibanha.bizmanager.mail.MailSender;
import me.ibanha.bizmanager.model.Request;
import me.ibanha.bizmanager.model.User;
import me.ibanha.bizmanager.model.UserDao;
import me.ibanha.bizmanager.model.control.Constructor;
import me.ibanha.bizmanager.model.control.Model;

public class EmailTemplate {
    private User admin;
    private User user;
    private Helper helper;
    private AppManager manager;
    private MailSender email;

    private String subject;
    private String body;
    private String sender;
    private String recipients;

    public EmailTemplate() {
        manager = AppManager.getInstance();
        FireDatabase firebase = manager.getFireDatabase();
        UserDao dao = manager.getRoom().getUserDao();

        admin = dao.getAdmin(true);
        user = dao.getByUid(firebase.getUserUid());
        helper = new Helper();
        email = new MailSender(admin.getEmail(), admin.getPassword());
    }

    public void composeEmailByConfirmedTemplate(Model item) {
        subject = "Підтвердження заявки";
        body = "Вашу заявку на " + item.getBuildedServicesIds() + " - прийнято.\n" +
                "Повна вартість послуги враховуючи знижку: " + item.getPricesWithDiscount() + ".\n" +
                "Чекаємо вас " + item.getDate() + ", о " + item.getTime() + ".";
        sender = admin.getEmail();
        recipients = item.getEmail();
    }

    public void composeEmailByDoneTemplate(Model item) {
        subject = "Про виконання заявки";
        body = "Вашу заявку на " + item.getBuildedServicesIds() + " - виконано.\n" +
                "Повна вартість послуги враховуючи знижку: " + item.getPricesWithDiscount() + ".\n" +
                "Дякуємо що обрали нас!";
        sender = admin.getEmail();
        recipients = item.getEmail();
    }

    public void composeEmailByNewRequestTemplate(Request request) {
        Constructor constructor = new Constructor();
        constructor.init(request.getId(), request.isApproved());
        Model model = constructor.getModel();

        subject = "Створено нову заявку";
        body = manager.getCurrentDate() + ", користувач " + user.getName() + " створив нову заявку," +
                " зазначивши такі послуги: " + model.getBuildedServicesIds() + ". " +
                "Повна вартість послуги враховуючи знижку: " + model.getPricesWithDiscount() +
                " грн" + ".\n" + " Заплановано на: " + request.getDate() + ". ";
        sender = admin.getEmail();
        recipients = user.getEmail();
    }

    public void send() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    email.sendMail(subject, body, recipients, sender);
                } catch (Exception e) {
                    Log.e("SendMail", e.getMessage(), e);
                }
            }

        }).start();
    }

    public void composeEmailByEditrequestTemplate(Request request) {
        Constructor constructor = new Constructor();
        constructor.init(request.getId(), request.isApproved());
        Model model = constructor.getModel();

        subject = "Змінено  заявку";
        body = manager.getCurrentDate() + ", користувач " + user.getName() + " змінив заявку," +
                " зазначивши такі послуги: " + model.getBuildedServicesIds() + ". " +
                "Повна вартість послуги враховуючи знижку: " + model.getPricesWithDiscount() +
                " грн" + ".\n" + " Заплановано на: " + request.getDate() + ". ";
        sender = admin.getEmail();
        recipients = user.getEmail();
    }
}
