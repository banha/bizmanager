package me.ibanha.bizmanager.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import me.ibanha.bizmanager.model.Service;
import me.ibanha.bizmanager.model.User;

public class Helper {
    public int calcDiscount(int discountVal, int price) {
        // Цена * Х / (100 + Х)
        int discount;

        int p = price * discountVal;
        int c = 100 + discountVal;
        discount = p / c;

        return discount;
    }

    public int calcSumWithDiscount(ArrayList<Service> services, User user) {
        int price = 0;
        for (int i = 0; services.size() > i; i++) {
            price = (int) (price + services.get(i).getPrice());
        }
        return price - calcDiscount(user.getDiscount(), price);
    }

    public String calculateSumOfHours(ArrayList<String> times) {
        StringBuilder state = new StringBuilder();
        int minutes = 0;
        for (int i = 0; times.size() > i; i++) {
            List<String> list = Arrays.asList(times.get(i).split(":"));
            minutes = minutes + ((Integer.parseInt((String) list.get(0)) * 60) +
                    Integer.parseInt((String) list.get(1)));
        }
        int hours = minutes / 60;
        minutes = minutes % 60;
        return state.append(hours).append(":").append(minutes).toString();
    }

    public String calculateSumOfHours(List<Service> times) {
        StringBuilder state = new StringBuilder();
        int minutes = 0;
        for (int i = 0; times.size() > i; i++) {
            List<String> list = Arrays.asList(times.get(i).getDuration().split(":"));
            minutes = minutes + ((Integer.parseInt((String) list.get(0)) * 60) +
                    Integer.parseInt((String) list.get(1)));
        }
        int hours = minutes / 60;
        minutes = minutes % 60;
        return state.append(hours).append(":").append(minutes).toString();
    }

    public int findValuePosition(String val, ArrayList<Service> list) {
        for (int i = 0; i < list.size(); i++) {
            if (val.equals(list.get(i).getId()))
                return i;
        }
        return -1;
    }

    public String collectTitle(List<String> list) {
        StringBuilder string = new StringBuilder();
        for (int i = 0; list.size() > i; i++) {
            if (list.size() == 1) {
                string.append(list.get(i));
            } else if (i == 0) {
                string.append(list.get(i)).append(", ");
            } else if (i == (list.size() - 1)) {
                string.append(list.get(i).toLowerCase());
            } else {
                string.append(list.get(i).toLowerCase()).append(", ");
            }
        }
        return string.toString();
    }

    public long calcSum(ArrayList<Service> services) {
        int price = 0;
        for (int i = 0; services.size() > i; i++) {
            price = (int) (price + services.get(i).getPrice());
        }
        return price;
    }
}
