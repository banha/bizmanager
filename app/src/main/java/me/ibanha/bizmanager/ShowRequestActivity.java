package me.ibanha.bizmanager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import me.ibanha.bizmanager.database.AppManager;
import me.ibanha.bizmanager.model.Request;
import me.ibanha.bizmanager.model.RequestDao;
import me.ibanha.bizmanager.model.control.Constructor;
import me.ibanha.bizmanager.model.control.Model;

public class ShowRequestActivity extends AppCompatActivity {

    private TextView name;
    private TextView phone;
    private TextView title;
    private TextView discount;
    private TextView commonPrice;
    private TextView priceWithDiscount;
    private TextView targetTime;
    private TextView duration;
    private Request request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_task);

        name = findViewById(R.id.show_request_client_name);
        phone = findViewById(R.id.show_request_phone);
        title = findViewById(R.id.show_request_title);
        discount = findViewById(R.id.show_request_discount);
        commonPrice = findViewById(R.id.show_request_price_without_discount);
        priceWithDiscount = findViewById(R.id.show_request_price_with_discount);
        targetTime = findViewById(R.id.show_request_target_time);
        duration = findViewById(R.id.show_request_duration_time);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onStart() {
        super.onStart();
        Map<String, Object> params = new HashMap<>();
        Intent intent = getIntent();

        params.put("id", intent.getStringExtra("id"));
        params.put("type", intent.getBooleanExtra("type", false));

        DataInit dataInit = new DataInit();
        dataInit.execute(params);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_show_service_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.btn_edit:
                Intent intent = new Intent(this, CreateRequestActivity.class);
                intent.putExtra("id", request.getId());
                intent.putExtra("type", request.isApproved());
                intent.putExtra("mode", 3);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onNavigateUp() {
        finish();
        return true;
    }

    @SuppressLint("StaticFieldLeak")
    private class DataInit extends AsyncTask<Map, Void, Model> {

        @Override
        protected Model doInBackground(Map... maps) {
            RequestDao dao = AppManager.getInstance().getRoom().getRequestDao();
            String id = (String) maps[0].get("id");
            Boolean type = (Boolean) maps[0].get("type");
            Constructor constructor = new Constructor();
            constructor.init(id, type);
            request = dao.getItemById(id);
            return constructor.getModel();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @SuppressLint("SetTextI18n")
        @Override
        protected void onPostExecute(Model model) {
            super.onPostExecute(model);
            name.setText(model.getName());
            phone.setText(model.getPhone());
            title.setText(model.getBuildedServicesIds());
            discount.setText(model.getDiscount().toString() + "%");
            commonPrice.setText(String.valueOf((int) model.getPricesWithoutDiscount()) + " грн");
            priceWithDiscount.setText(String.valueOf((int) model.getPricesWithDiscount()) + " грн");
            targetTime.setText(model.getTime() + " год");
            duration.setText(model.getDuration() + " год");

            setTitle("Від " + model.getDate());
        }
    }
}
