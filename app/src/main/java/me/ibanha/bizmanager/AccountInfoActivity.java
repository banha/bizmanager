package me.ibanha.bizmanager;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import me.ibanha.bizmanager.database.AppManager;
import me.ibanha.bizmanager.database.FireDatabase;
import me.ibanha.bizmanager.model.User;
import me.ibanha.bizmanager.model.UserDao;

public class AccountInfoActivity extends AppCompatActivity {

    private EditText name;
    private EditText phone;
    private TextView email;
    private TextView errName;
    private TextView errPhone;

    private FireDatabase firebase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_info);
        setTitle("Дані облікового запису");

        name = findViewById(R.id.edit_account_name);
        phone = findViewById(R.id.edit_account_phone);
        email = findViewById(R.id.account_email);
        errName = findViewById(R.id.account_error_name);
        errPhone = findViewById(R.id.account_error_phone);

        firebase = AppManager.getInstance().getFireDatabase();
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onStart() {
        super.onStart();
        email.setText(firebase.getUserEmail());
        Initializer initializer = new Initializer();
        initializer.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_new_universal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_save:
                save();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigateUp() {
        setResult(RESULT_CANCELED);
        finish();
        return true;
    }

    private void save() {
        deleteErrors();
        User user = new User();

        if (!name.getText().toString().isEmpty()) {
            user.setName(name.getText().toString());
        } else {
            errName.setText("Вкажіть ваше ім'я та прізвище!");
            return;
        }

        if (!phone.getText().toString().isEmpty()) {
            user.setPhone(phone.getText().toString());
        } else {
            errPhone.setText("Вкажіть ваш номер мобільного!");
            return;
        }

        user.setUid(firebase.getUserUid());
        user.setEmail(firebase.getUserEmail());
        user.setDiscount(0);
        user.setAdmin(false);

        Sender sender = new Sender();
        sender.execute(user);
    }

    private void deleteErrors() {
        errName.setText("");
        errPhone.setText("");
    }

    @SuppressLint("StaticFieldLeak")
    private class Sender extends AsyncTask<User, Void, Void> {

        @Override
        protected Void doInBackground(User... users) {
            UserDao dao = AppManager.getInstance().getRoom().getUserDao();
            FireDatabase firebase = AppManager.getInstance().getFireDatabase();
            User user = users[0];
            if (dao.getAdmin(true).getUid().equals(user.getUid())) {
                user.setAdmin(true);

                dao.deleteByUid(user.getUid());
                dao.insert(user);
                firebase.insertAdminInfo(user);
            } else {
                dao.deleteByUid(user.getUid());
                dao.insert(user);
                firebase.insertUserItem(user);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            finish();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class Initializer extends AsyncTask<Void, Void, Void> {
        User user;

        @Override

        protected Void doInBackground(Void... voids) {
            UserDao dao = AppManager.getInstance().getRoom().getUserDao();
            user = dao.getByUid(firebase.getUserUid());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (user != null) {
                name.setText(user.getName());
                phone.setText(user.getPhone());
            }
        }
    }
}
