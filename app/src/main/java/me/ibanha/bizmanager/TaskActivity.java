package me.ibanha.bizmanager;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import me.ibanha.bizmanager.database.AppManager;
import me.ibanha.bizmanager.database.FireDatabase;
import me.ibanha.bizmanager.database.FirestoreSaver;
import me.ibanha.bizmanager.database.FirestoreSaverCallback;
import me.ibanha.bizmanager.fragment.ConfFragment;
import me.ibanha.bizmanager.fragment.MyRequestsFragment;
import me.ibanha.bizmanager.fragment.ServiceFragment;
import me.ibanha.bizmanager.fragment.UnconfFragment;
import me.ibanha.bizmanager.model.User;
import me.ibanha.bizmanager.model.UserDao;

public class TaskActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private final String TAG = this.getClass().getSimpleName();

    private FireDatabase fire;

    private Button btnAuth;
    private DrawerLayout drawer;

    private ServiceFragment serviceFragment;
    private UnconfFragment unconfFragment;
    private ConfFragment confFragment;
    private MyRequestsFragment myRequestsFragment;
    private NavigationView navView;
    private Toolbar toolbar;
    private User user;
    private ProgressDialog dialog;

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        fire = AppManager.instance.getFireDatabase();

        if (fire.isSigned()) {
            SyncData task = new SyncData();
            task.execute();

        } else {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        navView = findViewById(R.id.nav_view);
        drawer = findViewById(R.id.drawer_layout);
    }

    @SuppressLint("CommitTransaction")
    @Override
    protected void onStart() {
        super.onStart();

        drawer.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                ImageView imageView = findViewById(R.id.img_nav_header);
                TextView textView = findViewById(R.id.txt_title_nav_header);

                btnAuth = findViewById(R.id.auth_btn);
                if (fire.isSigned()) {
                    Log.d(TAG, "Is Signed!");
                    btnAuth.setText(R.string.btn_auth_signed);
                    try {
                        imageView.setVisibility(View.VISIBLE);
                        textView.setText(user.getName());
                        textView.setVisibility(View.VISIBLE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d(TAG, "Is Not Signed!");
                    btnAuth.setText(R.string.btn_auth_not_signed);
                    imageView.setVisibility(View.INVISIBLE);
                    textView.setVisibility(View.INVISIBLE);
                }
            }
        });

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.task, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.task_action_settings:
                Intent settings = new Intent(this, SettingsActivity.class);
                startActivity(settings);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//        syncText.setVisibility(View.GONE);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        switch (item.getItemId()) {
            case R.id.nav_confirmed_tasks:
                transaction.replace(R.id.fragment_container, confFragment);
                break;
            case R.id.nav_unconfirmed_tasks:
                transaction.replace(R.id.fragment_container, unconfFragment);
                break;
            case R.id.nav_my_requests:
                transaction.replace(R.id.fragment_container, myRequestsFragment);
                break;
            case R.id.nav_services:
                transaction.replace(R.id.fragment_container, serviceFragment);
                break;
            case R.id.nav_report:
                Intent intent = new Intent(this, ReportActivity.class);
                startActivity(intent);
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        transaction.commit();
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.auth_btn:
                if (fire.isSigned()) {
                    fire.signOut();
                    Toast.makeText(this, "Signed Out!", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Intent intent = new Intent(this, LoginActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.btn_refresh:
                break;
        }
    }

    private void setUpUi() {
        if (user == null) {
            Intent intent = new Intent(getApplicationContext(), AccountInfoActivity.class);
            startActivity(intent);
        }
        Menu menu = navView.getMenu();
        try {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            TextView status = findViewById(R.id.textView11);
            if (!fire.isAdmin()) {
                myRequestsFragment = new MyRequestsFragment();
                transaction.replace(R.id.fragment_container, myRequestsFragment);
                menu.findItem(R.id.nav_confirmed_tasks).setVisible(false);
                menu.findItem(R.id.nav_unconfirmed_tasks).setVisible(false);
                menu.findItem(R.id.nav_my_requests).setVisible(true);
                status.setText("Користувач");
                status.setVisibility(View.VISIBLE);
            } else {
                transaction.replace(R.id.fragment_container, confFragment);
                status.setText("Адміністратор");
                status.setVisibility(View.VISIBLE);
            }
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class SyncData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            FirestoreSaverCallback callback = new FirestoreSaver();

            UserDao userDao = AppManager.getInstance().getRoom().getUserDao();

            fire.downloadAdminInfo(callback);
            fire.downloadUsers(callback);
            fire.downloadServices(callback);
            fire.downloadRequests(callback);

            serviceFragment = new ServiceFragment();
            confFragment = new ConfFragment();
            unconfFragment = new UnconfFragment();

            user = userDao.getByUid(fire.getUserUid());
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(TaskActivity.this);
            dialog.setTitle("Зачекайте");
            dialog.setCancelable(false);
            dialog.setMessage("Синхронізація даних");
            dialog.show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            setUpUi();
            dialog.setMessage("Синхронізацію завершено");
            dialog.dismiss();
        }
    }
}
